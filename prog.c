#include <stdio.h>               
#include <string.h>  
#include <stdlib.h> 

int main(int argc, char** argv){

	int consumidores = 0; 
	int productores = 0;
	int tamanoBuffer = 0;
	int elementos = 0;
	char *opcional = (char *)malloc(50);

	if(argc == 1){
		printf("Uso: ./prog -p <#> -c <#> -n <#> -e <#> [sync]\n");
		printf("-c nos dice el número de hilos consumidores\n");
		printf("-p nos dice el número de hilos productores\n");
		printf("-n el tamaño del buffer (arreglo)\n");
		printf("-e numero de elementos a producir\n");
		printf("sync, si existe, hará que el programa use semáforos\n");
		exit(-1);
	} 

	if(argc < 9){
		printf("La cantidad de argumentos es invalida\n");
		exit(-1);
	}

	if( (strcmp(argv[1], "-p") == 0) && (strcmp(argv[3], "-c") == 0) && (strcmp(argv[5], "-n") == 0) && (strcmp(argv[7], "-e") == 0) ){
		consumidores = atoi(argv[2]);
		productores = atoi(argv[4]);
		tamanoBuffer = atoi(argv[6]);
		elementos = atoi(argv[8]);
	}

	if(argc == 10){
		opcional = argv[9];
	} else {
		opcional = "-"; //No se agrego la bandera opcional sync
	}

	if( (strcmp(opcional, "sync") == 0) ){
		//CODIGO INDICANDO QUE EL PROGRAMA USA SEMAFOROS
	}

	printf("%d %d %d %d %s\n",consumidores,productores,tamanoBuffer,elementos, opcional);

	return 0;

}
