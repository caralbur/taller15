prog: prog.o
	gcc ./prog.o -o prog
prog.o: ./prog.c
	gcc -Wall -c ./prog.c -o prog.o


.PHONY: clean
clean:
	rm ./prog

